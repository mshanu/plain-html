function formatUtils() {
    function getWeekDay(date) {
        return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][date.getDay()];
    }

    function expandOrgName(org) {
        switch (org) {
            case 'dynamocamp':
                return 'Dynamo Camp';
            case 'vidas':
                return 'Vidas';

            default:
                return 'Organization A';
        }
    }

    return {
        formatDate: (date) => {
            return date.getDate() + ' April 2021 (' + getWeekDay(date) + ')';
        },

        joinOrgNames: (set) => {
            return [...set]
                .map(expandOrgName)
                .join(', ');
        }
    }
}
