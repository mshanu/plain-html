function availabilityClient() {
    // const gun = Gun({peers: ['https://e2eec.herokuapp.com/gun', 'https://mvp-gun.herokuapp.com/gun']})
    const gun = Gun({peers: ['https://mvp-gun.herokuapp.com/gun']})

    return {
        shareAvailability: (volunteer, dates, organization) => {
            dates.forEach(d => {
                console.log('Submitting date', volunteer, d.getTime(), organization);

                gun.get('volunteerCalendars')
                    .get(organization)
                    .get(d.getTime())
                    .get(volunteer)
                    .put("available")
            })
        },

        withdrawAvailability:(volunteer, dates, organization) => {
            dates.forEach(d => {
                console.log('Withdrawing date', volunteer, d.getTime(), organization);

                gun.get('volunteerCalendars')
                    .get(organization)
                    .get(d.getTime())
                    .get(volunteer)
                    .put(null, ack => {
                        console.log('Was withdrawal successful?', organization, ack);
                    })
            })
        },

        onUserAvailability: (cb) => {
            const orgs = ['dynamocamp', 'vidas'];
            orgs.forEach(org => {
                let dates = gun.get('volunteerCalendars').get(org);
                dates.map().on((value, date) => {
                    const volunteerName = Object.keys(value)[1];
                    const availedDate = new Date(Number(date));
                    const available = !!value[volunteerName];
                    cb({volunteerName, availedDate, organization: org, available });
                })
            })
        }
    }
}
