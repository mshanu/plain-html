function calendar(renderDate = undefined) {
    if (!renderDate) {
        renderDate = defaultDateRenderer;
    }
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    const $ = (tagName, textContent = undefined) => {
        const result = document.createElement(tagName);
        if (textContent) {
            const textNode = document.createTextNode(textContent);
            result.appendChild(textNode);
        }
        return result;
    };

    const clickListeners = [];

    function defaultDateRenderer(cell, date) {
        while (cell.lastChild) {
            cell.removeChild(cell.lastChild);
        }
        cell.appendChild($('span', date));
    }

    function createTableHeader() {
        const days = ['Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab', 'Dom'];
        const header = $('thead');
        const row = $('tr');
        row.className = 'o3cal-week-days';
        header.appendChild(row);

        for (day in days) {
            row.appendChild($('th', days[day]));
        }

        return header;
    }

    function createTableBody() {
        const todayDate = today.getDate();

        function getDateClass(date) {
            if (date < todayDate) {
                return 'past-date';
            }
            if (date === todayDate) {
                return 'today';
            }
            return 'future-date';
        }

        const firstDay = ((new Date(year, month, 1)).getDay() + 6) % 7;
        const daysInMonth = countDaysInMonth(month, year);

        const tbl = $('tbody');
        // creating all cells

        let date = 1;
        let timeClassName = getDateClass(date);

        for (let i = 0; i < 6; i++) {
            if (date > daysInMonth) {
                break;
            }

            let row = $('tr');

            for (let j = 0; j < 7; j++) {
                if (date > daysInMonth) {
                    break;
                }

                if (i === 0 && j < firstDay) {
                    row.appendChild($('td'));
                } else {
                    const cell = $('td');
                    cell.setAttribute('data-date', date);
                    cell.setAttribute('data-month', month + 1);
                    cell.setAttribute('data-year', year);
                    cell.setAttribute('data-relative', timeClassName);
                    cell.className = 'o3cal-date-cell ' + timeClassName;
                    const cellDate = new Date(year, month, date);
                    cell.addEventListener('click', event => {
                        onCellClick(cell, cellDate);
                    });

                    renderDate(cell, date);

                    row.appendChild(cell);
                    timeClassName = getDateClass(++date);
                }
            }

            tbl.appendChild(row);
        }

        return tbl;
    }

    function onCellClick(cell, date) {
        const event = new CustomEvent('cellclick', {detail: {cell: cell, date: date}});
        clickListeners.forEach(listener => listener(event));
    }

    function countDaysInMonth(m, y) {
        return 32 - new Date(y, m, 32).getDate();
    }

    function createCalendarTable() {
        const table = $('table');
        table.className = 'o3cal-table-calendar';

        table.appendChild(createTableHeader());
        table.appendChild(createTableBody(month, year));
        return table;
    }

    function findCellByDate(date) {
        return document.querySelector(`[data-date="${date}"]`);
    }

    return {
        findCellByDate,

        appendTo: (element) => {
            element.appendChild(createCalendarTable());
        },

        addCellClickListener: (listener) => {
            clickListeners.push(listener);
        },

        addLabel: (date, labelText) => {
            let element = findCellByDate(date);
            if (element) element.appendChild($('p', labelText));
        },

        refresh: (date) => {
            let element = findCellByDate(date);
            if (element) renderDate(element, date);
        },

        disable: (date, hint) => {
            let element = findCellByDate(date);
            if (!element) return;
            element.classList.add('disabled');
            let newElement = element.cloneNode(true);
            newElement.setAttribute('title', hint);
            newElement.classList.remove('selected');
            element.parentNode.replaceChild(newElement, element);
        },

        enable: (date) => {
            let element = findCellByDate(date);
            if (!element) return;
            element.classList.remove('disabled');
            element.removeAttribute('title');
        }
    };
}
